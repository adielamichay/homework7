import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable} from 'rxjs';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class TodosService {

  deleteTodo(key:string){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').remove(key);
    })
  }
  

  addTodo(text:string){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').push({'text':text});
    })
  }
  
  update(key,text){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text});
    })
  }
  user:Observable<firebase.User>;

  constructor(private db:AngularFireDatabase, 
              private authService:AuthService) { 
                
              }
 
  
}