import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import {TodosService } from '../todos.service';


@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  todoTextFromTodo='No todos task so far';
  todos = [ ];
  text:string;
  key;
  

 

  addTodo(){
    this.todosService.addTodo(this.text);
    this.text = '';

  }
  toDelete(){
    this.todosService.deleteTodo(this.key);
  }

  
  constructor(private router:Router,
              private authService:AuthService,
              private db:AngularFireDatabase,
              private todosService:TodosService,
             ) 
              { 
            }
          
  ngOnInit() {
    console.log(this.todos.length)
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{
          this.todos = [];
          todos.forEach(
            todo => {
              let y =todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
              
            }
          )
        }
      ) 
    })
  }

}
